const mongoose = require("mongoose")
 
const userSchema = new mongoose.Schema({

	firstname: {
		type: String,
		required: [true, "First name is required"]
	},
	lastname: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email name is required"]
	},
	password: {
		type: String		
	},
	loginType: {
		type: String,
		required: [true, "Login Type is required"]
	},
	category: [{		
		categoryName: {
			type: String,
			required: [true, 'Category Name is required']
		},	
		categoryType: {
			type: String
		}				
	}],
	balance: {
		type: Number,
		default: 0
	},
	records: [{
		categoryName: {
			type: String
		},
		categoryType: {
			type: String
		},
		amount: {
			type: Number
		},
		description: {
			type: String
		},
		date: {
			type: Date,
			default: new Date()
		},
		currentBalance: {
			type: Number,
			default: 0
		}
	}]
})

module.exports = mongoose.model('user', userSchema)