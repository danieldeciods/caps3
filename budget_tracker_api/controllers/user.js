const User = require("../models/user")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const { OAuth2Client } = require('google-auth-library')
const clientId = '1042993511505-d27s3fdfhe4pfluauvf0cu4dn77g0or6.apps.googleusercontent.com'
const errCatcher = err => console.log(err)


module.exports.emailExists = (parameterFromRoute) => {

	return User.find({email : parameterFromRoute.email }).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})
	.catch(errCatcher)
}

module.exports.register = (params) => {
	let newUser = new User({
		firstname: params.firstname,
		lastname: params.lastname,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'regular'
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
	})
	.catch(errCatcher)
}

module.exports.login = (params) => {
	return User.findOne({email : params.email})
	.then(user => {
		if (user === null) return false

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if(isPasswordMatched){
			return {accessToken: auth.createAccessToken(user.toObject())}
		} else {
			return false
		}
	})
	.catch(errCatcher)
}


module.exports.getPrivate = (params) => {
	return User.findById(params.id)
	.then(user => {
		user.password = undefined
		return user 
	})
	.catch(errCatcher)
}


module.exports.verifyGoogleTokenId = async(tokenId) => {

	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
	
	if(data.payload.email_verified === true){

		const user = await User.findOne({ email: data.payload.email }).exec()
		
		if( user !== null ){
			
			if(user.loginType === 'google'){				
				return{ access: auth.createAccessToken(user.toObject())}
			} else {
				return { error: 'login-type-error' }
			}

		} else {
			const newUser = new User({
				firstname: data.payload.given_name,
				lastname: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})

			return newUser.save().then((user, err) => {
				
				return{ access: auth.createAccessToken(user.toObject())}
			})
		}
	} else {
		return { error: 'google-auth-error' }
	}
}


module.exports.addCategory = (params) => {
	/*console.log(params)*/
	return User.findById(params.userId)
	.then(result => {
		result.category.push({
			categoryName: params.categoryName,
			categoryType: params.categoryType
		})
		return result.save().then((result, err) => err ? false : true)
	})
}


module.exports.get = () => {
	return User.find({}).then(result => result)
}


module.exports.addRecord = (params) => {

	let updatedUser= {
			balance: params.balance
		}
		return User.findByIdAndUpdate(params.userId, updatedUser)
		.then((user, err) => {
			return User.findById(params.userId)
			.then(result => {			
				result.records.push({
					categoryName: params.categoryName,
					categoryType: params.categoryType,
					amount: params.amount,
					description: params.description,
					currentBalance: params.currentBalance
				})
				
				return result.save().then((result, err) => err? false : true)
			})
		
		})
	}