const express = require("express")
const router = express.Router()
const auth = require("../auth")
const UserController = require("../controllers/user")


router.post("/email-exists", (req, res) => {
	UserController.emailExists(req.body).then(result => res.send(result))
})

router.post("/", (req, res) => {
	UserController.register(req.body).then(result => res.send(result))
})

router.post("/login", (req, res) => {
	UserController.login(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.getPrivate(user).then(user => res.send(user))
})

router.post('/verify-google-id-token', async (req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

//add category
router.post("/addCategory", auth.verify, (req, res) => {
	let params = {
		userId: auth.decode(req.headers.authorization).id,
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType
	}
	UserController.addCategory(params).then(result => res.send(result))
})


router.get("/details", (req,res) => {
	UserController.get().then((result) => res.send(result))
})


//add record
router.post("/addRecord", auth.verify, (req, res) => {
	let params = {
		userId: auth.decode(req.headers.authorization).id,
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType,
		amount: req.body.amount,
		description: req.body.description,
		balance: req.body.balance,
		currentBalance: req.body.currentBalance
	}
	UserController.addRecord(params).then(result => res.send(result))
})



module.exports = router