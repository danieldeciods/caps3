const express = require("express")
const app = express()
require('dotenv').config()
const mongoose = require('mongoose')
const cors = require('cors')
const port = process.env.PORT
const userRoutes = require('./routes/user')
const mongodbCloud = process.env.DB_MONGODB
const corsOptions = {
	origin: ['http://localhost:3000'],
	optionsSuccessStatus: 200
}


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Cloud'))
mongoose.connect(mongodbCloud, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

app.use('/api/users', cors(corsOptions), userRoutes)


app.listen(port, () => console.log(`Listening to port ${port}`))